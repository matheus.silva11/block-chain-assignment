package com.avenuecode.blockchain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class BlockchainApplication {
	
	public static int prefix = 4;

	public static void main(String[] args) {

		ArrayList<Block> blockchain = new ArrayList<Block>(); 
		
		
		ArrayList<Block> blockList = new ArrayList<Block>(); 

		blockList.add(new Block("Hi im the first block", "0", new Date().getTime()));		
		blockList.add(new Block("Yo im the second block",blockList.get(blockList.size()-1).getHash(), new Date().getTime())); 
		blockList.add(new Block("Hey im the third block",blockList.get(blockList.size()-1).getHash(), new Date().getTime()));
		
		ExecutorService executorService = Executors.newFixedThreadPool(blockList.size());
		
		List<Future<?>> futuresList = new ArrayList<>();
				
		blockList.forEach(block -> {
			Future future = executorService.submit(newCallable(block));
			futuresList.add(future);
		});
				
		futuresList.forEach(future -> {
            try {
            	
            	Block block = (Block) future.get();
            	blockchain.add(block);
            	
            	System.out.println("Block: ");
            	System.out.println("Hash of the previous block: ");
            	System.out.println(block.getPreviousHash());
            	System.out.println("Hash of the block mined: ");
            	System.out.println(block.getHash());
            	System.out.println("Block data: ");
            	System.out.println(blockList.get(blockchain.size()-1).getData());
            	System.out.println();
            } catch (InterruptedException e) {
            	System.out.println("Task interrupted: " + e.getMessage());
                Thread.currentThread().interrupt();
            } catch (ExecutionException e) {
            	System.out.println("Error on task execution: " + e.getMessage());
            }
        });
		
		executorService.shutdown();


	}
	

	private static Callable newCallable(Block block) {
		return new Callable() {
			public Object call() {
				block.mineBlock(prefix);
				return block;
			}
		};
	}


}
